# Drawing SVG plots 

Our best shot at plotting graphs of the devices' usage might be SVG. Good thing is that the server can take care of drawing them plots through Jinja templating. 

Proposed plots:

* Timeline of availability over time (with "connected" and "plugged" sub-series for mobiles).
* Pie chart of proportion of the number of available devices over time.

Resources: 

* [Some SVG documentation](https://www.tutorialspoint.com/svg/)
* [CSS-Tricks SVG charts demo](https://css-tricks.com/how-to-make-charts-with-svg/)
* [Graphs with SVG](https://rvlasveld.github.io/blog/2013/07/02/creating-interactive-graphs-with-svg-part-1/)
* [Simple pie charts with CSS](https://www.smashingmagazine.com/2015/07/designing-simple-pie-charts-with-css/)