**Add datum**
----

Send a datum from a device to the server.

* **URL**

    `/api/add_datum`

* **Method**

    `POST`
    
*  **URL Params**

    None.

* **POST Params**

    **Required:**

    * `user_token=[string]`

        The user's private token.

        Must exist in the database.

    * `device_token=[string]`

        The device's token.

    * `device_type=[string]`

        The device's type (e.g. laptop, mobile, desktop...).

    * `event_type=[string]`

        The event's type (`connected`, `disconnected`, `plugged_in` or `plugged_off`).

    * `local_time=[string]`

        Local time according to the device.

        **Must be formatted in ISO-8601 date format.**

        `local_time` will be parsed by [dateutil.isoparse](https://dateutil.readthedocs.io/en/stable/parser.html?highlight=isoparse#dateutil.parser.isoparse). Check their docs for format hints.

* **Success Response:**

    * **Code:** 201 CREATED

        **Content:** `{ "message" : "Successfully added datum" }`

        Your contribution was saved to the database.
 
* **Error Response:**

    * **Code:** 403 FORBIDDEN

        **Content:** `{ "error" : "Invalid user_token" }` OR `{ "error" : "Duplicate record" }`

        Either the `user_token` is invalid, or the same record was already saved on the server.

    * **Code:** 422 UNPROCESSABLE ENTRY

        **Content:** `{ "error" : "Invalid field",  "field": "<the_field>"}`

        Possible reasons:

        * The provided `local_time` does not parse into a valid ISO-8601 date.

        The other fields perform no verification.

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 
