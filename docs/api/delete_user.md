**Delete user**
----

Request deletion of an existing user from the server.
This is the first of two phases for user deletion, where the client asks for a random `nonce` that will have to be sent back to the server to confirm user deletion.
See [`deleted_user`](deleted_user.md) for the second phase.

* **URL**

    `/api/delete_user`

* **Method**

    `POST`
    
*  **URL Params**

    None.

* **Data Params**

    **Required:**
 
    * `user_token=[string]`

        A valid user's private token. 

* **Success Response:**
    
    * **Code:** 200 OK
    
        **Content:** `{ "nonce" : "a-random-string" }`

        The server provided a `nonce`, that will need to be sent back to [`deleted_user`](./deleted_user.md) to confirm user deletion.
 
* **Error Response:**

    * **Code:** 403 FORBIDDEN

        **Content:** `{ "error" : "Invalid user_token" }`

    <!-- * **Code:** 429 TOO MANY REQUEST

        **Content:** `{ "error" : "You should stop spamming" }`

        The client (identified by IP) performed to many `delete_user` requests and the server won't accept new ones until the end of a timeout. -->

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 

* **Notes:**

    <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._> 