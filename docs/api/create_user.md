**Create user**
----

Request an unassigned user token from the server. 
This is the first of two phases for user registration, where the client asks for an unassigned `user_token` from the server. See [`created_user`](created_user.md) for the second phase.

* **URL**

    `/api/create_user`

* **Method**

    `GET`
    
*  **URL Params**

    None.

* **Data Params**

    None.

* **Success Response:**

    * **Code:** 200 OK
    
        **Content:** `{ "user_token" : "they-prison-debate-high-pause-analyst" }`

        The server provided a `user_token`. 

        But **the user will not be actually registered until the client sends a [`created_user`](created_user.md) request with the same `user_token`**.
 
* **Error Response:**

    * **Code:** 429 TOO MANY REQUEST

        **Content:** `{ "error" : "You should stop spamming" }`

        The client (identified by IP) performed to many `create_user` requests and the server won't accept new ones until the end of a timeout.

* **Sample Call:**

    ```bash
    $ curl https://esquads.zinz.dev/api/create_user
    { "user_token" : "they-prison-debate-high-pause-analyst" }
    ```

* **Notes:**

    `create_user` is the first phase of a two-phase user registration process:

    * The client gets an unreserved private user token (`user_token`) from the server by calling `create_user`.
    
    * If the user completes the account creation, the client sends a [`created_user`](created_user.md) request to the server containing the same `user_token` and optional additional fields.

    More robust authentication mechanisms could be envisioned:

    * https://en.wikipedia.org/wiki/Challenge-Handshake_Authentication_Protocol
    * https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol