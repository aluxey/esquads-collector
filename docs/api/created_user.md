**Created user**
----

Complete the creation of a new user by POSTing a `user_token` acquired from [`create_user`](./create_user.md) and optional additional user information.

This is the second of two phases for user registration, where the client confirms their registration. See [`create_user`](./create_user.md) for the first phase.

* **URL**

    `/api/created_user`

* **Method**
    
    `POST`
    
*  **URL Params**

    None.

* **Data Params**

    **Required:**
 
    * `user_token=[string]`

        The user's private token.

        Must have been previously acquired from [`create_user`](./create_user.md).


    **Optional:**
 
    * `email=[string]`

        The user's email address.

* **Success Response:**
    
    <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_>

    * **Code:** 201 CREATED

        **Content:** `{ "message": "Created user" }`

 
* **Error Response:**

    * **Code:** 403 FORBIDDEN

        **Content:** `{ "error" : "Invalid field",  "field": "<the_field>"}`

        Possible reasons:

        * The `user_token` was not provided by a previous call to [`create_user`](./create_user.md).
        * The `email` does not parse into a valid e-mail address.

    * **Code:** 408 REQUEST TIMEOUT

        **Content:** `{ "error" : "You took too long" }`

        The time between [`create_user`](./create_user.md) and `created_user` exceeded timeout length (5 minutes). Repeat the process.

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 

* **Notes:**

    <_TODO_>: We will need to forbid having several tokens for the same e-mail. Otherwise we cannot e-mail the token back to the user on demand (which token, if there are several?).