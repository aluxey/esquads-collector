**Get data**
----
Retrieve all past data provided by the user (identified by `user_token`).
Can be used for e.g. plotting graphs of the user's activity on the client.

This should be **cacheable**. _TODO_: implement a request cache!

* **URL**

    `/api/get_data`

* **Method**

    `POST`
    
*  **URL Params**

    None.

* **Data Params**

    **Required:**

    * `user_token=[string]`

        The user's private token.

        Must exist in the database.

* **Success Response:**

    * **Code:** 200 OK

        **Content:** 

        ```json 
        { 
            "user_token": "they-prison-debate-high-pause-analyst",
            "email": "joe@doe.com",
            "events": [
                {
                    "user_token": "they-prison-debate-high-pause-analyst",
                    "device_token": "chou-farci",
                    "device_type": "laptop",
                    "event_type": "connected",
                    "local_time": "2021-02-01 08:00:00"
                },
                {
                    "user_token": "they-prison-debate-high-pause-analyst",
                    "device_token": "chou-farci",
                    "device_type": "laptop",
                    "event_type": "disconnected",
                    "local_time": "2021-02-01 09:00:00"
                }
            ]
        }`

        Returns the same information as [`get_user`](./get_user.md), plus an `events` item that contains the list of the user's recorded events.

        **Each `event` is consituted of the same fields as the ones provided in [`add_datum`](./add_datum.md)**:

        * `user_token`: the user's private token,
        * `device_token`: the token of the device that triggered the event,
        * `device_type`: the device's type (e.g. laptop, mobile, desktop...),
        * `event_type` the event's type (`connected`, `disconnected`, `plugged_in` or `plugged_off`),
        * `local_time`: the event's timestamp according to the device's local clock at the time that the event was triggered.

        The results are ordered by `device_token` and ascending `local_time`.
 
* **Error Response:**

    * **Code:** 403 FORBIDDEN

        **Content:** `{ "error" : "Invalid user_token" }`

        The user-s private token does not exist in the database.

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 

* **Notes:**

    <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._> 