**Deleted user**
----


Complete the deletion of a user by POSTing their `user_token` alongside the random `nonce`  acquired from [`delete_user`](./delete_user.md).

This is the second of two phases for user deletion, where the client confirms the user suppression. See [`delete_user`](./delete_user.md) for the first phase.

* **URL**

    `/api/deleted_user`

* **Method**
    
    `POST`
    
*  **URL Params**

    None.

* **Data Params**

    **Required:**
 
    * `user_token=[string]`
        
        A user's private token.
 
    * `nonce=[string]`
        
        The random `nonce` that was provided by the server upon calling [`delete_user`](./delete_user.md) with the same `user_token` as above.

* **Success Response:**

    * **Code:** 200 OK
        
        **Content:** `{ "message" : "Deleted user" }`

        The user was successfully deleted.
 
* **Error Response:**

    * **Code:** 403 FORBIDDEN

        **Content:** `{ "error" : "Invalid field",  "field": "<the_field>"}`

        Possible reasons:

        * The `user_token` was not provided by a previous call to [`delete_user`](./delete_user.md).
        * The `nonce` does not correspond to the one provided for this `user_token` by [`delete_user`](./delete_user.md).


    * **Code:** 408 REQUEST TIMEOUT

        **Content:** `{ "error" : "You took too long" }`

        The time between [`delete_user`](./delete_user.md) and `deleted_user` exceeded timeout length (5 minutes). Repeat the process.

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 

* **Notes:**

    <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._> 