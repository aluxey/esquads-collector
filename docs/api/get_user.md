**Get user**
----

Get user information based on the provided `user_token`.

_TODO_: Better security!

* **URL**

    `/get_user`

* **Method**
    
    `POST`
    
*  **URL Params**

    None.

* **Data Params**

    **Required:**

    * `user_token=[string]`

        The user's private token.

        Must exist in the database.

* **Success Response:**
    
    * **Code:** 200 OK

        **Content:** 

        ```json 
        { 
            "user_token": "they-prison-debate-high-pause-analyst",
            "email": "joe@doe.com"
        }`

        `email` field is always returned, but equals `""` if not provided by the user.
 
* **Error Response:**

    **Content:** `{ "error" : "Invalid user_token" }`

        The user-s private token does not exist in the database.

* **Sample Call:**

    <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._> 

* **Notes:**

    <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._> 