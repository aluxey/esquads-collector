from flask import (
    Blueprint
)
from flask_restful import Api

from esquads_backend.resources.add_datum import AddDatum
from esquads_backend.resources.create_user import CreateUser
from esquads_backend.resources.created_user import CreatedUser
from esquads_backend.resources.delete_user import DeleteUser
from esquads_backend.resources.deleted_user import DeletedUser
from esquads_backend.resources.get_data import GetData
from esquads_backend.resources.get_user import GetUser


bp = Blueprint('api', __name__)
api = Api(bp, catch_all_404s=True)

api.add_resource(AddDatum, '/add_datum')
api.add_resource(CreatedUser, '/created_user')
api.add_resource(CreateUser, '/create_user')
api.add_resource(DeletedUser, '/deleted_user')
api.add_resource(DeleteUser, '/delete_user')
api.add_resource(GetData, '/get_data')
api.add_resource(GetUser, '/get_user')
