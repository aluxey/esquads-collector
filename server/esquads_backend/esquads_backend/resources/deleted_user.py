from flask_restful import Resource, reqparse

from esquads_backend import db
from esquads_backend.models import User, PendingUserDeletion
from esquads_backend.utils.parameters import add_user_token, add_nonce
from esquads_backend.utils.time import past_timeout, REST_TIMEOUT

req_parser = reqparse.RequestParser()
add_user_token(req_parser)
add_nonce(req_parser)


class DeletedUser(Resource):
    def post(self):
        args = req_parser.parse_args()
        token = args.user_token
        nonce = args.nonce

        # If token not in PendingUserDeletion table, return 403
        pending_user = PendingUserDeletion.query.get(token)
        if pending_user is None:
            return {"error": "Invalid field",  "field": "user_token"}, 403
        elif pending_user.nonce != nonce:
            return {"error": "Invalid field",  "field": "nonce"}, 403
        elif past_timeout(pending_user.created, REST_TIMEOUT):
            db.session.delete(pending_user)
            db.session.commit()
            return {"error": "You took too long"}, 408

        user = User.query.get(token)
        db.session.delete(user)
        db.session.delete(pending_user)
        db.session.commit()

        return {"message": "Deleted user"}, 200
