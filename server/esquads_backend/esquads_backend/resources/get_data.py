#from flask import jsonify
import json
from flask_restful import Resource, reqparse

# from esquads_backend import db
from esquads_backend.models import User, Event
from esquads_backend.utils.parameters import add_user_token
from esquads_backend.utils.token import is_valid_token
from esquads_backend.utils.user import user_exists

req_parser = reqparse.RequestParser()
add_user_token(req_parser)


class GetData(Resource):
    def post(self):
        args = req_parser.parse_args()
        user_token = args.user_token

        # if not user_exists(user_token):
        #     return {"error": "Invalid user_token"}, 403

        user = User.query.get(user_token)
        if user is None:
            return {"error": "Invalid user_token"}, 403

        data = user.serialise()
        data['events'] = Event.serialise_list(user.entries, short=True)

        print(data)
        return data, 200

        #events = Event.query.filter_by(user_id=user_token).all()
        # return Event.serialise_list(events), 200
