from flask_restful import Resource, reqparse

from esquads_backend.utils.parameters import add_user_token
from esquads_backend.utils.token import is_valid_token
from esquads_backend.utils.user import user_exists
from esquads_backend.models import User

req_parser = reqparse.RequestParser()
add_user_token(req_parser)


class GetUser(Resource):
    def post(self):
        args = req_parser.parse_args()
        user_token = args.user_token

        # if not user_token or not is_valid_token(user_token):
        #     return {"error": "Invalid user_token"}, 403

        user = User.query.get(user_token)
        if user is None:
            return {"error": "Invalid user_token"}, 403

        return user.serialise(), 200
