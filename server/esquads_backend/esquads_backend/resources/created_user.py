from flask_restful import Resource, reqparse

from esquads_backend import db
from esquads_backend.models import User, PendingUserCreation
from esquads_backend.utils.parameters import \
    add_user_token, add_user_email
from esquads_backend.utils.time import past_timeout, REST_TIMEOUT

req_parser = reqparse.RequestParser()
add_user_token(req_parser)
add_user_email(req_parser)


class CreatedUser(Resource):
    def post(self):
        args = req_parser.parse_args()
        user_token = args.user_token
        email = args.email
        if email is not None and email == '':
            email = None

        # Already checked by reqparser (see `add_user_email` function)
        # if email is not None and not validate_email(email):
        #     return {"error": "Invalid field",  "field": "email"}, 403

        # If user_token not in PendingUserCreation table, return 403
        pending_user = PendingUserCreation.query.get(user_token)
        if pending_user is None:
            return {"error": "Invalid field",  "field": "user_token"}, 403
        elif past_timeout(pending_user.created, REST_TIMEOUT):
            db.session.delete(pending_user)
            db.session.commit()
            return {"error": "You took too long"}, 408

        user = User(id=user_token, email=email)
        db.session.add(user)
        db.session.delete(pending_user)
        db.session.commit()

        return {"message": "Created user"}, 201
