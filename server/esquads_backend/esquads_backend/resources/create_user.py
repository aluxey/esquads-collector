from flask_restful import Resource

from esquads_backend import db
from esquads_backend.models import PendingUserCreation
from esquads_backend.utils.token import generate_unique_token


class CreateUser(Resource):
    def get(self):
        token = generate_unique_token()

        pending_user = PendingUserCreation(user_id=token)
        db.session.add(pending_user)
        db.session.commit()

        return {"user_token": token}, 200
