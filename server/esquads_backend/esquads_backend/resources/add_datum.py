from flask_restful import Resource, reqparse

from esquads_backend import db
from esquads_backend.models import Event
from esquads_backend.utils.parameters import \
    add_user_token, add_device_token, add_device_type, \
    add_event_type, add_local_time
from esquads_backend.utils.token import is_valid_token
from esquads_backend.utils.user import user_exists

req_parser = reqparse.RequestParser(bundle_errors=True)
add_user_token(req_parser)
add_device_token(req_parser)
add_device_type(req_parser)
add_event_type(req_parser)
add_local_time(req_parser)


class AddDatum(Resource):
    def post(self):
        args = req_parser.parse_args()

        if not user_exists(args.user_token):
            return {"error": "Invalid user_token"}, 403

        event = Event(
            user_id=args.user_token,
            device_id=args.device_token,
            device_type=args.device_type,
            event_type=args.event_type,
            device_timestamp=args.local_time)

        db.session.add(event)
        db.session.commit()

        return {"message": "Successfully added datum"}, 201
