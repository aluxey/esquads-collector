from flask_restful import Resource, reqparse

from esquads_backend import db
from esquads_backend.models import PendingUserDeletion
from esquads_backend.utils.parameters import add_user_token
from esquads_backend.utils.user import user_exists
from esquads_backend.utils.nonce import generate_nonce

req_parser = reqparse.RequestParser()
add_user_token(req_parser)


class DeleteUser(Resource):
    def post(self):
        args = req_parser.parse_args()
        token = args.user_token

        if not user_exists(token):
            return {"error": "Invalid user_token"}, 403

        nonce = generate_nonce()

        pending_user = PendingUserDeletion(user_id=token, nonce=nonce)
        db.session.add(pending_user)
        db.session.commit()

        return {"nonce": nonce}, 200
