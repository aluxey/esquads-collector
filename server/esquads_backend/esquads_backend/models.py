import json
from sqlalchemy import PrimaryKeyConstraint
from esquads_backend.utils.time import my_utcnow
from esquads_backend.utils.nonce import NONCE_SIZE

from esquads_backend import db


# TODO: custom type to verify tokens
token_type = db.String(80)


class User(db.Model):
    id = db.Column(token_type, primary_key=True)
    email = db.Column(db.String(120), unique=False, nullable=True)
    created = db.Column(db.DateTime(timezone=True),
                        nullable=False, default=my_utcnow)
    # `backref='user'` adds a `.user` field to Event (python) objects
    # `lazy=True` populates `.user` only when it is first accessed
    entries = db.relationship(
        'Event', backref='user', lazy=True,
        order_by='[Event.device_id, Event.device_timestamp]')

    def serialise(self):
        return {
            'user_token': self.id,
            'email': self.email if self.email is not None else ""
        }

    def __repr__(self):
        return json.dumps(self.serialise())


class Event(db.Model):
    user_id = db.Column(token_type, db.ForeignKey('user.id'), nullable=False)
    device_id = db.Column(token_type, nullable=False)
    device_type = db.Column(db.Text(), nullable=False)
    event_type = db.Column(db.Text(), nullable=False)
    device_timestamp = db.Column(db.DateTime, nullable=False)
    created = db.Column(db.DateTime(timezone=True),
                        nullable=False, default=my_utcnow)
    # status = db.Column(db.Enum(AvailEnum), nullable=False,
    #                    default=AvailEnum.ongoing)
    # The tuple <'device_id', 'device_timestamp'> is the primary key
    __table_args__ = (
        PrimaryKeyConstraint('device_id', 'device_timestamp'),)

    def serialise(self, short=False):
        if short:
            return {
                'device_token': self.device_id,
                'device_type': self.device_type,
                'event_type': self.event_type,
                'local_time': str(self.device_timestamp)}
        else:
            return {
                'user_token': self.user_id,
                'device_token': self.device_id,
                'device_type': self.device_type,
                'event_type': self.event_type,
                'local_time': str(self.device_timestamp),
                'server_time': str(self.created)}

    @staticmethod
    def serialise_list(events, short=False):
        return [e.serialise(short) for e in events]

    def __repr__(self):
        return json.dumps(self.serialise())


class PendingUserCreation(db.Model):
    user_id = db.Column(token_type, primary_key=True)
    created = db.Column(db.DateTime, nullable=False, default=my_utcnow)


class PendingUserDeletion(db.Model):
    user_id = db.Column(token_type, primary_key=True)
    nonce = db.Column(db.String(NONCE_SIZE), unique=True, nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=my_utcnow)
