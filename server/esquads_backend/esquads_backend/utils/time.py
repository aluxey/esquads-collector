from datetime import datetime, timezone, timedelta

REST_TIMEOUT = timedelta(minutes=5)


def my_utcnow():
    """Returns datetime.utcnow without tzinfo

    Every database field stores dates using this format: UTC w/o tzinfo
    """
    return datetime.now(timezone.utc).replace(tzinfo=None)


def past_timeout(d, timeout_length):
    now = my_utcnow()
    return (now - d) >= timeout_length
