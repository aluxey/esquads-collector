from mnemonic import Mnemonic

from esquads_backend.utils.user import user_exists

LANG = 'english'
MNEMO_LENGTH = 6
mnemo = Mnemonic(LANG)


def generate_token():
    """Generate a user token
    """

    token = mnemo.generate()
    token = '-'.join(token.split(' ')[:MNEMO_LENGTH])
    return token


def generate_unique_token():
    """Generate a *unique* user token by checking the database.
    """

    while True:
        token = generate_token()

        # Break once token not in db
        if not user_exists(token):
            break

    return token


def is_valid_token(token):
    """Verify that the token is made of the expected characters
    """

    alphabet = "abcdefghijklmnopqrstuvwxyz-"
    if token is None:
        return False
    if not isinstance(token, str):
        return False
    if len(token) == 0:
        return False

    for c in token:
        if c not in alphabet:
            return False

    return True


def token_type(token):
    """Verify a token's syntactic validity
    """
    if is_valid_token(token):
        return token
    else:
        raise ValueError('Not a valid token')
