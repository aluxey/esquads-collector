import uuid

NONCE_SIZE = 32  # characters


def generate_nonce():
    return uuid.uuid4().hex


def is_valid_nonce(nonce):
    try:
        uuid.UUID(hex=nonce, version=4)
    except ValueError:
        return False
    else:
        return True


def nonce_type(nonce):
    """Verify a nonce's syntactic validity
    """
    if is_valid_nonce(nonce):
        return nonce
    else:
        raise ValueError('Not a valid nonce')
