from esquads_backend.models import User


def user_exists(token):
    """Verify that the token exists in the User table
    """
    return User.query.get(token) is not None
