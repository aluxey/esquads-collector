from validate_email import validate_email


def email_type(email):
    """Verify a email's syntactic validity
    """
    if validate_email(email):
        return email
    else:
        raise ValueError('Not a valid e-mail address')
