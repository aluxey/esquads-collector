from dateutil.parser import isoparse

from esquads_backend.utils.token import token_type
from esquads_backend.utils.email import email_type
from esquads_backend.utils.nonce import nonce_type


def add_user_token(parser):
    parser.add_argument(
        'user_token', dest='user_token', type=token_type,
        required=True, location='form',
        help="The user's private token",
    )


def add_user_email(parser):
    parser.add_argument(
        'email', dest='email', type=email_type,
        required=False, location='form',
        help="The user's email address (optional)",
    )


def add_device_token(parser):
    parser.add_argument(
        'device_token', dest='device_token', type=token_type,
        required=True, location='form',
        help="The device's token",
    )


def add_device_type(parser):
    parser.add_argument(
        'device_type', dest='device_type', type=str,
        required=True, location='form',
        help="The device's type (e.g. laptop, mobile, desktop...)",
    )


def add_event_type(parser):
    parser.add_argument(
        'event_type', dest='event_type', type=str,
        required=True, location='form',
        help="The event's type (connected, disconnected, plugged_in or plugged_off)",
    )


def add_local_time(parser):
    parser.add_argument(
        'local_time', dest='local_time', type=isoparse,
        required=True, location='form',
        help='Local time according to the device, formatted as ISO-8601',
    )


def add_nonce(parser):
    parser.add_argument(
        'nonce', dest='nonce', type=nonce_type,
        required=True, location='form',
        help='A random string previously provided by the server',
    )
