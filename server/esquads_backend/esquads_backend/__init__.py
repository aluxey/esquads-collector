import os
import click

from flask import Flask
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import sqlalchemy

# Database ORM
db = SQLAlchemy()
# Database migration toolkit (to update the DB when we modify the schema)
migrate = Migrate()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    click.echo("########### Creating database ###########")
    db.create_all()
    click.echo('Initialized the database.')


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__,
                instance_path=os.environ.get('FLASK_INSTANCE_PATH'),
                instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.environ['FLASK_SECRET_KEY'],
        SQLALCHEMY_DATABASE_URI='sqlite:///' + os.path.join(
            app.instance_path, 'esquads_backend.orm.sqlite'),
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.cli.add_command(init_db_command)
    app.cli.add_command(populate_db_command)

    db.init_app(app)
    migrate.init_app(app, db)

    from . import api
    app.register_blueprint(api.bp)

    return app


@click.command('populate-db')
@with_appcontext
def populate_db_command():
    click.echo("########### Populating database ###########")
    db.create_all()

    sql_fn = os.environ.get('INITIAL_DATA')
    if sql_fn is None or sql_fn == "":
        click.echo((
            "Set the `INITIAL_DATA` environment variable "
            "to the path of an SQL file."))
        return

    click.echo(f'Loading SQL from {os.getcwd()}/{sql_fn}...')
    with open(sql_fn, 'r') as f, db.engine.connect() as c:
        for l in f.readlines():
            c.execute(sqlalchemy.text(l))

    print("Populated the database.")
