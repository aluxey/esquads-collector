import os
import tempfile
import pytest

from esquads_backend import create_app, db
from esquads_backend.utils.nonce import generate_nonce, is_valid_nonce
from esquads_backend.utils.token import generate_token, is_valid_token


@pytest.fixture(scope='module')
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///' + db_path,
    })

    with app.app_context():
        db.create_all()

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture(scope='module')
def client(app):
    return app.test_client()


# @pytest.fixture(scope='module')
# def runner(app):
#     return app.test_cli_runner()


def _create_user(client):
    resp = client.get('/create_user')
    assert resp.is_json
    assert resp.status_code == 200
    j = resp.get_json()
    assert 'user_token' in j
    return j['user_token']


def _created_user(client, user_token, email=None,
                  is_correct_token=True):
    # Plaintext POST query
    # For json, replace `data` parameter name by `json`
    data = {'user_token': user_token}
    if email is not None and email != "":
        data['email'] = email
    resp = client.post('/created_user', data=data)
    if is_correct_token:
        assert resp.status_code == 201
    elif is_valid_token(user_token):
        assert resp.status_code == 403
        raise ValueError
    else:
        assert resp.status_code == 400
        raise ValueError


def _delete_user(client, user_token, is_correct_token=True):
    resp = client.post('/delete_user',
                       data={'user_token': user_token})
    assert resp.is_json
    if is_correct_token:
        assert resp.status_code == 200
        j = resp.get_json()
        assert 'nonce' in j
        return j['nonce']
    elif is_valid_token(user_token):
        assert resp.status_code == 403
        raise ValueError
    else:
        assert resp.status_code == 400
        raise ValueError


def _deleted_user(client, user_token, nonce,
                  is_correct_token=True, is_correct_nonce=True):
    resp = client.post('/deleted_user',
                       data={
                           'user_token': user_token,
                           'nonce': nonce})
    if is_correct_token and is_correct_nonce:
        assert resp.status_code == 200
    elif is_valid_token(user_token) and is_valid_nonce(nonce):
        assert resp.status_code == 403
        raise ValueError
    else:
        assert resp.status_code == 400
        raise ValueError


def test_created_user(client):
    user_token = _create_user(client)
    _created_user(client, user_token)


def test_created_user_wrong_user_token(client):
    user_token = _create_user(client)
    user_token = generate_token()
    with pytest.raises(ValueError):
        _created_user(client, user_token, is_correct_token=False)


def test_created_user_invalid_user_token(client):
    user_token = _create_user(client)
    user_token += "&à_0"
    with pytest.raises(ValueError):
        _created_user(client, user_token, is_correct_token=False)


def test_deleted_user(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    nonce = _delete_user(client, user_token)
    _deleted_user(client, user_token, nonce)


def test_delete_user_wrong_user_token(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    user_token = generate_token()
    with pytest.raises(ValueError):
        _delete_user(client, user_token, is_correct_token=False)


def test_delete_user_invalid_user_token(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    user_token += "&à_0"
    with pytest.raises(ValueError):
        _delete_user(client, user_token, is_correct_token=False)


def test_deleted_user_wrong_user_token(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    nonce = _delete_user(client, user_token)
    user_token = generate_token()
    with pytest.raises(ValueError):
        _deleted_user(client, user_token, nonce,
                      is_correct_token=False)


def test_deleted_user_wrong_user_token(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    nonce = _delete_user(client, user_token)
    user_token += "&à_0"
    with pytest.raises(ValueError):
        _deleted_user(client, user_token, nonce,
                      is_correct_token=False)


def test_deleted_user_wrong_nonce(client):
    from esquads_backend.utils.nonce import generate_nonce
    user_token = _create_user(client)
    _created_user(client, user_token)
    nonce = _delete_user(client, user_token)
    nonce = generate_nonce()
    with pytest.raises(ValueError):
        _deleted_user(client, user_token, nonce,
                      is_correct_nonce=False)


def test_deleted_user_invalid_nonce(client):
    user_token = _create_user(client)
    _created_user(client, user_token)
    nonce = _delete_user(client, user_token)
    nonce += "lalala"
    with pytest.raises(ValueError):
        _deleted_user(client, user_token, nonce,
                      is_correct_nonce=False)


def test_get_user_without_email(client):
    user_token = _create_user(client)
    _created_user(client, user_token)

    resp = client.post('/get_user',
                       data={'user_token': user_token})
    assert resp.is_json
    assert resp.status_code == 200
    j = resp.get_json()
    assert 'user_token' in j
    assert j['user_token'] == user_token
    assert 'email' in j
    assert j['email'] == ""


def test_get_user_with_email(client):
    user_token = _create_user(client)
    email = "jon@doe.com"
    _created_user(client, user_token, email)

    resp = client.post('/get_user',
                       data={'user_token': user_token})
    assert resp.is_json
    assert resp.status_code == 200
    j = resp.get_json()
    assert 'user_token' in j
    assert j['user_token'] == user_token
    assert 'email' in j
    assert j['email'] == email


def test_add_datum_get_data(client):
    # Create a user
    user_token = _create_user(client)
    _created_user(client, user_token)

    # Generate some data
    device_token = generate_token()
    device_type = "laptop"
    sent_data = {
        "user_token": user_token,
        "device_token": device_token,
        "device_type": device_type,
        "event_type": "connected",
        "local_time": "2021-02-01 08:00:00"
    }

    # Upload the data
    resp = client.post('/add_datum', data=sent_data)
    assert resp.is_json
    assert resp.status_code == 201

    # Fetch the data
    resp = client.post('/get_data', data={'user_token': user_token})
    assert resp.is_json
    assert resp.status_code == 200
    j = resp.get_json()
    assert 'user_token' in j
    assert j['user_token'] == user_token
    assert 'email' in j
    assert j['email'] == ""
    assert 'events' in j
    events = j['events']
    assert len(events) == 1
    assert events[0] == sent_data
