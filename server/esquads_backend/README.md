# Esquads Collector's backend

## Installing & Running

The Python dependencies are listed in `requirements.txt`. Install them locally e.g. using `pip`, preferably using some kind of virtual environment:

	pip install -r requirements.txt

Then, source the `ENV.debug` file to get debug environment variables and run Flask locally:

	source ENV.debug 
	# Do this once to initialise the database:
	flask init-db 
	# Or, if you also want to populate the DB with dummy test data:
	flask populate-db 
	# Run the server:
	flask run

## API Docs

Refer to `/docs/api` directory.

## Testing 

### Unit tests 

Some API tests were written in `tests/`. To execute them, run:

```bash 
python3 -m pytest 
```

### Dummy data 

To start working on data visualisation before we have working clients generating data, we provide means to populate the database with dummy data.

First ensure that `ENV.debug` contains an `export INITIAL_DATA=...` that points to a valid SQL file in this directory (e.g. `initial_data.sqlite`).

Then call `flask populate-db`. If the CLI complains about duplicate entries, you can always **delete the entire database file** by doing the following:

	# WARNING: Destroys the database 
	# Do it only for testing purposes on a database file you don't care about 
	rm instance/esquads_backend.orm.sqlite

