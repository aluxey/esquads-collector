from flask import (
    Blueprint, render_template, abort, flash
)
from jinja2 import TemplateNotFound

bp = Blueprint('static', __name__)


@bp.route('/', defaults={'page': 'index'})
@bp.route('/<page>')
def show(page):
    flash(
        "This project is under active development and not ready to use.",
        "warning")
    try:
        return render_template('static/%s.html' % page)
    except TemplateNotFound:
        abort(404)
