import functools
from flask import (
    Blueprint, render_template, redirect,
    flash, session, g, request, url_for
)
# from jinja2 import TemplateNotFound

from esquads_frontend.api import get_user, create_user, created_user, get_data
from esquads_frontend._gen_svg_timeline import gen_svg_timeline

bp = Blueprint('user', __name__, url_prefix='/user')


###################################
# Decorators and helper functions #
###################################


@bp.before_app_request
def load_logged_in_user():
    g.user = None
    user_token = session.get('user_token')
    if user_token:
        try:
            user = get_user(user_token)
        except Exception as e:
            flash(str(e))
            # Avoid error firing multiple times
            session.clear()
        else:
            g.user = user


def login_required(view):
    """
    Wrap view funcs in login_required to redirect to login if not connected
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('user.login'))

        return view(**kwargs)

    return wrapped_view


#####################
# Route definitions #
#####################


@bp.route('/register', methods=('GET', 'POST'))
def register():
    error = None
    user_token = None

    # If the user filled the form
    if request.method == 'POST':
        user_token = request.form['user_token']
        email = request.form['email']

        try:
            created_user(user_token, email)
        except Exception as e:
            error = str(e)
            # Clear session
            session.clear()
            # flash(f"Registration failed with code {r.status_code}: {r.text}")
        else:
            # Renew session with new user
            session.clear()
            session['user_token'] = user_token
            load_logged_in_user()
            return render_template(
                'user/register.html',
                created=True)

    user_token = session.get('user_token')
    # If the user is not logged in
    if user_token is None:
        # Get a token from the backend
        try:
            ans = create_user()
        except Exception as e:
            error = str(e)
        else:
            user_token = ans.get('user_token')
            # Seems stupid:
            # Renew session with new user
            # session.clear()
            # session['user_token'] = user_token

    if error is not None:
        flash(error)
    load_logged_in_user()
    return render_template(
        'user/register.html',
        created=False,
        user_token=user_token)


@bp.route('/login', methods=('GET', 'POST'))
def login():
    error = None
    if request.method == 'POST':
        user_token = request.form['user_token']
        try:
            user = get_user(user_token)
        except Exception as e:
            error = str(e)
        else:
            session.clear()
            session['user_token'] = user_token
            # Should be called automatically due to
            # `@bp.before_app_request` decorator:
            # load_logged_in_user()
            return redirect(url_for('user.profile'))

    if error is not None:
        flash(error)
    return render_template('user/login.html')


@bp.route('/logout')
def logout():
    session.clear()
    flash('You are now logged out.')
    return redirect(url_for('static.show', page='index'))


@bp.route('/send_token', methods=('GET', 'POST'))
def send_token():
    error = "Sending mails is not yet implemented. :("
    if request.method == 'POST':
        error = ("Enter your e-mail address all you want, "
                 "we won't implement this feature faster.")

    if error is not None:
        flash(error)
    return render_template('user/send_token.html')


@bp.route('/profile')
@login_required
def profile():
    try:
        data = get_data(g.user['user_token'])
    except Exception as e:
        flash(str(e))
    else:
        svg_timeline = gen_svg_timeline(data)
    return render_template(
        'user/profile.html',
        events=data['events'],
        svg_timeline=gen_svg_timeline(data))
