import os
from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.environ['FLASK_SECRET_KEY'],
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Registering blueprints
    from . import static, errors, user
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(403, errors.not_allowed)
    app.register_error_handler(500, errors.internal_server_error)
    app.register_blueprint(static.bp)
    app.register_blueprint(user.bp)

    return app
