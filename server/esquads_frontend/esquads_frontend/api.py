import requests
import os


def get_user(user_token):
    r = requests.post(
        url=f"{os.environ['API_ADDR']}/get_user",
        data={
            "user_token": user_token
        })

    if r.status_code != 200:
        # r.text contains the error message
        raise _to_runtime_error(r)
    return r.json()


def get_data(user_token):
    r = requests.post(
        url=f"{os.environ['API_ADDR']}/get_data",
        data={
            "user_token": user_token
        })

    if r.status_code != 200:
        # r.text contains the error message
        raise _to_runtime_error(r)
    return r.json()


def create_user():
    r = requests.get(url=f"{os.environ['API_ADDR']}/create_user")

    if r.status_code != 200:
        raise _to_runtime_error(r)
    return r.json()


def created_user(user_token, email=None):
    if email is not None and email == '':
        email = None

    r = requests.post(
        url=f"{os.environ['API_ADDR']}/created_user",
        data={"user_token": user_token, "email": email})

    if r.status_code != 201:
        raise _to_runtime_error(r)
    return r.json()


def _to_runtime_error(r):
    return RuntimeError(f"Error {r.status_code}: {r.text}")
