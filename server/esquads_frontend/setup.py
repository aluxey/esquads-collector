from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='esquads_frontend',
    packages=find_packages(),
    version='0.3',
    author='Adrien Luxey',
    author_email='adrien@luxeylab.net',
    description='Data collection server measuring e-squads\' availability' +
                ' - frontend',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://git.deuxfleurs.fr/adrien/esquads_collector',
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask>=1.1',
        'requests',
        'pandas'
    ],
)
