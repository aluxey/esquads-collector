package io.dufour.quentin.esquad;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import io.github.novacrypto.bip39.*;
import io.github.novacrypto.bip39.wordlists.English;

import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    SharedPreferences hndl;
    SharedPreferences.Editor hndl_ed;

    EditText input_user_token;
    EditText input_device_token;
    Spinner input_device_type;
    Button input_switch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        input_user_token = findViewById(R.id.user_token);
        input_device_token =  findViewById(R.id.device_token);
        input_device_type = findViewById(R.id.device_type);
        input_switch = findViewById(R.id.tracking_switch);

        hndl = getPreferences(Context.MODE_PRIVATE);
        hndl_ed = hndl.edit();

        updateStatus();
        if (isTrackingActivated()) activatePeriodicWorker(false);
        hydrate();
    }

    private void updateStatus() {
        TextView st = findViewById(R.id.tracking_info);
        st.setText(R.string.worker_not_registered);

        WorkManager
                .getInstance(this)
                .getWorkInfosForUniqueWorkLiveData("probe")
                .observe(this, workInfo -> {
                    if (workInfo == null || workInfo.size() == 0) {
                        st.setText(R.string.worker_not_registered);
                        return;
                    }
                    if (workInfo.size() > 1) {
                        st.setText(R.string.worker_logic_error);
                        return;
                    }

                    WorkInfo wi = workInfo.get(0);
                    if (wi.getState() == WorkInfo.State.BLOCKED)
                        st.setText(R.string.worker_blocked);
                    else if (wi.getState() == WorkInfo.State.CANCELLED)
                        st.setText(R.string.worker_cancelled);
                    else if (wi.getState() == WorkInfo.State.ENQUEUED)
                        st.setText(R.string.worker_enqueued);
                    else if (wi.getState() == WorkInfo.State.FAILED)
                        st.setText(R.string.worker_enqueued);
                    else if (wi.getState() == WorkInfo.State.RUNNING)
                        st.setText(R.string.worker_running);
                    else if (wi.getState() == WorkInfo.State.SUCCEEDED)
                        st.setText(R.string.worker_succeeded);
                });
    }

    private OneTimeWorkRequest checkProbe() {
        Data data = new Data.Builder()
                .putString("user_token", getUserToken())
                .putString("device_token", getDeviceToken())
                .putString("device_type", getDeviceType())
                .build();

        OneTimeWorkRequest check = new OneTimeWorkRequest.Builder(ProbeWorker.class)
                .setInputData(data)
                .build();

        WorkManager
                .getInstance(this)
                .enqueueUniqueWork("check", ExistingWorkPolicy.REPLACE, check);

        return check;
    }

    private void activatePeriodicWorker(boolean replace) {
        Constraints constraints = new Constraints.Builder()
                //.setRequiresCharging(true)
                //.setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        Data data = new Data.Builder()
                .putString("user_token", getUserToken())
                .putString("device_token", getDeviceToken())
                .putString("device_type", getDeviceType())
                .putBoolean("background", true)
                .build();

        /* We can't probe more often with WorkManager.
         * If you still want to reduce the probing interval,
         * you will need to replace it by a Foreground Service. */
        PeriodicWorkRequest probe =
                new PeriodicWorkRequest
                        .Builder(ProbeWorker.class, 15, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .setInputData(data)
                        .build();

        WorkManager
                .getInstance(this)
                .enqueueUniquePeriodicWork(
                        "probe",
                        replace ? ExistingPeriodicWorkPolicy.REPLACE : ExistingPeriodicWorkPolicy.KEEP,
                        probe);
    }

    private void deactivatePeriodicWorker() {
        WorkManager.getInstance(this).cancelUniqueWork("probe");
    }

    private Boolean isTrackingActivated() {
        return hndl.getBoolean("is_tracking", false);
    }

    private String getUserToken() {
        return hndl.getString("user_token", "");
    }

    private String getDeviceToken() {
        String sto = hndl.getString("device_token", null);
        if (sto == null) {
            StringBuilder sb = new StringBuilder();
            byte[] entropy = new byte[Words.TWELVE.byteLength()];
            new SecureRandom().nextBytes(entropy);
            new MnemonicGenerator(English.INSTANCE).createMnemonic(entropy, sb::append);
            String[] words = sb.toString().split(" ");
            String token = words[0];
            for (int i = 1; i < 6; i++) {
                token += "-" + words[i];
            }
            sto = token;
            hndl_ed.putString("device_token", sto);
            hndl_ed.apply();
        }

        return sto;
    }

    private String getDeviceType() {
        String[] dev_array = getResources().getStringArray(R.array.device_array);
        return hndl.getString("device_type", dev_array[0]);
    }

    public void hydrate() {
        input_user_token.setText(getUserToken());
        input_device_token.setText(getDeviceToken());

        String[] dev_array = getResources().getStringArray(R.array.device_array);
        int sel_dev = Arrays.asList(dev_array).indexOf(getDeviceType());
        input_device_type.setSelection(sel_dev);

        if (isTrackingActivated()) {
            input_switch.setBackgroundColor(Color.GRAY);
            input_switch.setText(R.string.deactivate_track);
        } else {
            input_switch.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            input_switch.setText(R.string.activate_track);
        }

        input_switch.setEnabled(true);
    }

    public void onClickTrackingSwitch(View v) {
        input_switch.setEnabled(false);
        input_switch.setText(R.string.checking);

        hndl_ed.putString("user_token", input_user_token.getText().toString());
        hndl_ed.putString("device_type", input_device_type.getSelectedItem().toString());
        hndl_ed.apply();

        if (!isTrackingActivated()) {
            // Activate Tracking
            OneTimeWorkRequest check = checkProbe();
            WorkManager
                    .getInstance(this)
                    .getWorkInfoByIdLiveData(check.getId())
                    .observe(this, workInfo -> {
                        if (workInfo == null) return;
                        if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                            hndl_ed.putBoolean("is_tracking", true);
                            hndl_ed.apply();
                            hydrate();
                            activatePeriodicWorker(true);
                            return;
                        }
                        if (workInfo.getState() == WorkInfo.State.FAILED) {
                            int err = workInfo.getOutputData().getInt("err", R.string.error_unknown);
                            Toast.makeText(this, getResources().getText(err), Toast.LENGTH_LONG).show();
                            hndl_ed.putBoolean("is_tracking", false);
                            hndl_ed.apply();
                            hydrate();
                            return;
                        }
                    });
            //activatePeriodicWorker(true);
        } else {
            // Deactivate Tracking
            hndl_ed.putBoolean("is_tracking", false);
            hndl_ed.apply();

            deactivatePeriodicWorker();
            hydrate();
        }

    }
}
