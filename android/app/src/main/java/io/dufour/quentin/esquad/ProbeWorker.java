package io.dufour.quentin.esquad;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ProbeWorker extends Worker {
    public ProbeWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        if(getInputData().getBoolean("background", false)) {
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = getApplicationContext().registerReceiver(null, ifilter);
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            // We don't probe if we don't charge
            if (!isCharging) return Result.success();
        }

        RequestFuture<String> future = RequestFuture.newFuture();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = "https://esquads.zinz.dev/api/collect";

        StringRequest req = new StringRequest(Request.Method.POST, url, future, future) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> pars = new HashMap<String, String>();
                pars.put("Content-Type", "application/x-www-form-urlencoded");
                return pars;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                String now = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US).format(new Date());

                Map<String, String> pars = new HashMap<String, String>();
                pars.put("user_token", getInputData().getString("user_token"));
                pars.put("device_token", getInputData().getString("device_token"));
                pars.put("device_type", getInputData().getString("device_type"));
                pars.put("local_time", now);
                return pars;
            }
        };
        queue.add(req);
        Data.Builder out = new Data.Builder();

        try {
            String res = future.get(120, TimeUnit.SECONDS);
            out.putString("res", res);
            return Result.success(out.build());
        } catch (InterruptedException e) {
            out.putInt("err", R.string.error_interruption);
            return Result.failure(out.build());
        } catch(TimeoutException e) {
            out.putInt("err", R.string.error_timeout);
            return Result.failure(out.build());
        } catch(ExecutionException e) {
            out.putInt("err", R.string.error_execution);
            if (VolleyError.class.isAssignableFrom(e.getCause().getClass())) {
                VolleyError ve = (VolleyError) e.getCause();
                if (ve.networkResponse.statusCode == 404) {
                    out.putInt("err", R.string.error_404);
                } else if (ve.networkResponse.statusCode == 400) {
                    out.putInt("err", R.string.error_400);
                } else if (ve.networkResponse.statusCode == 500) {
                    out.putInt("err", R.string.error_500);
                }
            }
            return Result.failure(out.build());
        }
    }
}
