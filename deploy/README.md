# Deploy the server, man, please

This script:

1. Packages the `esquads_collector` package located in `/server`
2. Runs a Docker image/container on the remote with the server package 

## Dependencies

* Ansible
* `docker` & `docker-compose` (for [`community.general.docker_compose`](https://docs.ansible.com/ansible/latest/collections/community/general/docker_compose_module.html))
* Python packages:

	* [Docker package](https://docker-py.readthedocs.io/en/stable/) (for [`community.general.docker_compose`](https://docs.ansible.com/ansible/latest/collections/community/general/docker_compose_module.html))
	* PyYAML (for [`community.general.docker_compose`](https://docs.ansible.com/ansible/latest/collections/community/general/docker_compose_module.html))
* Ansible collections listed in `requirements.yml`. You just need to run:

		ansible-galaxy install -r requirements.yml

## Assumptions

* You are logged into the Docker Hub with username `particallydone`. Otherwise rename the Docker images in `/server/docker-compose.yml` and `deploy/templates/docker-compose.yml.j2`.
* You have access to the remote listed in `inventory`. 

## Run it 

	ansible-playbook deploy.yml -i inventory

## Yeah but what does it not do?

* Build the project's Docker images locally 
* Upload them to Docker Hub 
* Pull them on the remote 
* Run the project on the remote 
* Update nginx's configuration

What is 

* Create and configure user `esquads` on the remote:
	
	* The lil' dude is configured with `--disabled-password` such that you can only connect to it being root on the remote, or having your SSH key in its `authorized_keys`.

	* It needs to have the right to `sudo` without password. On my Debian, I had to add the following line to the *very end* of `/etc/sudoers`:

		esquads ALL=(ALL) NOPASSWD: ALL

	* It needs to be in `docker` group, such that we can do Docker thingies with it. 

* Get a SSL certificate with certbot. My script simply does not allow it, I'll figure a way to automate this someday.


## Using Docker Hub

Right now, I'm using the Docker Hub to push images from the local before pulling them on the remote. The uploaded images are publicly available, which sucks.

One day, when I'm grown-up, I'd like to host a good Docker registry like [Harbor](https://goharbor.io/).


